#include <stdio.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_rng.h>
#include "mygsl_matops.h"

gsl_rng *r; //global generator
int main (void)
{

	const gsl_rng_type *T;
	double sigma = 0.1;
	
	T = gsl_rng_default;
	r = gsl_rng_alloc(T);	
	
	gsl_matrix *m = gsl_matrix_alloc(10,3);
	gsl_matrix_set_all_gaus(m, r, sigma); 
	
	FILE * f = fopen ("test.dat", "w"); 
	gsl_matrix_fprintf (f,m, "%.5g"); 
	fclose (f);
}


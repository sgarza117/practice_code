#include <gsl/gsl_matrix.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

void gsl_matrix_set_all_gaus(gsl_matrix *m, const gsl_rng *r, double sigma) 
{

	size_t num_rows = m->size1;
	size_t num_cols = m->size2;
	double temp;
	printf ("generator type: %s\n", gsl_rng_name (r));	
	
	for (int i = 0; i < num_rows; i++)
	{
		for (int j = 0; j < num_cols; j++)
		{
			temp = gsl_ran_gaussian(r,sigma);
			gsl_matrix_set (m, i,j,temp);
		}
	}
	return;

}

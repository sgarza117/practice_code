#include <gsl/gsl_matrix.h>
#include <gsl/gsl_rng.h>

void gsl_matrix_set_all_gaus(gsl_matrix *m, const gsl_rng *r, double sigma); 

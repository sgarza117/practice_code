#include <gsl/gsl_vector.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <math.h>

void  add_vects(gsl_vector *v, const gsl_vector * v1,const gsl_vector * v2)
{
	size_t num_elements = v->size;
	double temp;
	
	for (int i = 0; i < num_elements; i++)
	{
		temp = gsl_vector_get (v1, i) + gsl_vector_get (v2, i);
		gsl_vector_set (v, i,temp);
	}
	return;

}

void mult_vects_elementwise(gsl_vector * v,const gsl_vector * v1,const gsl_vector * v2)
{
	size_t num_elements = v->size;
	double temp;
	
	for (int i = 0; i < num_elements; i++)
	{
		temp = gsl_vector_get (v1, i) * gsl_vector_get (v2, i);
		gsl_vector_set (v, i,temp);
	}
}


void gsl_vector_set_all_gaus(gsl_vector *v, const gsl_rng *r, double sigma)
{

	size_t num_elements = v->size;
	double temp;
	printf ("generator type: %s\n", gsl_rng_name (r));	
	
	for (int i = 0; i < num_elements; i++)
	{
		temp = gsl_ran_gaussian(r,sigma);
		gsl_vector_set (v, i,temp);
	}
	return;

}

void gsl_vector_set_all_range(gsl_vector *v, double start, double  end, double step)
{

	double  nsteps = (end - start)/step + 1; //double needed. int results in inaccuracies
	nsteps = rint(nsteps);                   //rounds to nearest int value. 

	double temp = start;
	
	for (int i = 0; i < nsteps; i++)
	{
		gsl_vector_set (v, i,temp);
		temp = temp + step;
	}
	
	return;

}

void scale_vect(gsl_vector *v, const gsl_vector * v1,double sf)
{
	size_t num_elements = v->size;
	double temp;
	
	for (int i = 0; i < num_elements; i++)
	{
		temp = gsl_vector_get (v1, i) * sf; 
		gsl_vector_set (v, i,temp);
	}
	return;

}

void bias_vect(gsl_vector *v, const gsl_vector * v1,double bias)
{
	size_t num_elements = v->size;
	double temp;
	
	for (int i = 0; i < num_elements; i++)
	{
		temp = gsl_vector_get (v1, i) + bias; 
		gsl_vector_set (v, i,temp);
	}
	return;

}

void sin_vect(gsl_vector *v, const gsl_vector * v1)
{
	size_t num_elements = v->size;
	double temp;
	
	for (int i = 0; i < num_elements; i++)
	{
		temp = sin(gsl_vector_get (v1, i)); 
		gsl_vector_set (v, i,temp);
	}
	return;
}

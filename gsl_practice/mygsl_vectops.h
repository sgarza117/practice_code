#include <gsl/gsl_vector.h>
#include <gsl/gsl_rng.h>

void add_vects(gsl_vector *v, const gsl_vector * v1,const gsl_vector * v2);
void mult_vects_elementwise(gsl_vector * v,const gsl_vector * v1,const gsl_vector * v2);
void gsl_vector_set_all_gaus(gsl_vector *v, const gsl_rng *r, double sigma); 
void scale_vect(gsl_vector *v, const gsl_vector * v1,double sf);
void bias_vect(gsl_vector *v, const gsl_vector * v1,double bias);
void gsl_vector_set_all_range(gsl_vector *v, double start, double end, double step);
void sin_vect(gsl_vector *v, const gsl_vector * v1);

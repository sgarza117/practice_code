#include <stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include "mygsl_vectops.h"
#include <math.h>

#define VECT_SIZE 10000
gsl_rng *r; //global generator

typedef struct 
{
	gsl_vector *v;
} MYSTRUCT;

int main (void) 
{	
	const gsl_rng_type *T;
	double sigma = 0.1;
	double norm;
	double dprod;
	double  start = 0;
	//double  end = 2*3.14159;
	double end;
	double  step = 0.01;
	double freq = 5;
	double phase = 3.14159/2;

	printf("Enter the end point for a vect: ");
	scanf("%lf",&end);
	int nsteps = (end-start)/step + 1;
	double nsteps2 = (end-start)/step + 1;
	printf("nsteps: %d\n",nsteps);
	printf("nsteps2: %f\n",nsteps2);

//	gsl_rng_env_setup();
	T = gsl_rng_default;
	
	r = gsl_rng_alloc(T);	
	printf ("generator type: %s\n", gsl_rng_name (r));	
	printf ("seed = %lu\n", gsl_rng_default_seed);
	

	gsl_vector * v = gsl_vector_calloc (VECT_SIZE);
	gsl_vector * v1 = gsl_vector_calloc (VECT_SIZE);
	gsl_vector * v2 = gsl_vector_calloc (VECT_SIZE);
	gsl_vector * v3 = gsl_vector_calloc (rint(nsteps2));
	gsl_vector * v4 = gsl_vector_calloc (rint(nsteps2));
	MYSTRUCT hello;
	hello.v = gsl_vector_calloc(v4->size);

	printf("MYSTRUCT hello has a gsl vector size: %zu\n",hello.v->size);

	gsl_vector_set_all_gaus(v3,r,sigma);
	gsl_vector_set_all_range(v4,start,end,step);

	scale_vect(v4,v4,freq); bias_vect(v4,v4,phase);
	sin_vect(v4,v4); add_vects(v4,v4,v3);
	gsl_vector_memcpy(hello.v,v4);
	
	int test = .5/.25; 
	printf("The floor of 10/3 is %d\n",test);

	//gsl_vector_set_all(v,5.0);
	gsl_vector_set_all(v1,4.0);
	printf("The first element of v is %f\n",gsl_vector_get(v,0));
//	v2 = add_vects(add_vects(v1,v2),add_vects(v,v3));
	//v2 = ((v+2)*5 + v1 .* v3)	
	bias_vect(v2,v,2);
	scale_vect(v2,v2,5);
	add_vects(v2,v2,v1);
	//mult_vects_elementwise(v2,v2,v3);
	
	printf("The size of v2 is %zu\n",v2->size);
//	mult_vects_elementwise(v2,v,v1);

	printf("The size of v is %zu\n",v->size);

/*
	for (int i = 0; i < 3; i++) // prints first 3 elements of a vector 
	{
		printf ("v_%d = %g\n", i, gsl_vector_get (v, i)); 
		printf ("v1_%d = %g\n", i, gsl_vector_get (v1, i)); 
	}
*/
	norm = gsl_blas_dnrm2(v1); //compute Eculid norm
	gsl_blas_ddot(v,v1,&dprod);//compute dot of v and v1, store in dprod
	
	printf("Norm of v1 is %f\n", norm);
	printf("Dot product of v and v1 is %f\n", dprod);

	//Write v2 to file to verify addition
	FILE * f = fopen ("test.dat", "w"); 
	gsl_vector_fprintf (f, hello.v, "%.5g"); 
	fclose (f);

	//Free memory used by vectors
	gsl_vector_free (v);
	gsl_vector_free (v1);
	gsl_vector_free (v2);
	gsl_rng_free(r);
	return 0;
}

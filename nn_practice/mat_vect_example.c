#include <stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include "mygsl_vectops.h"
#include "mygsl_matops.h"
#include <math.h>

int main (void)
{
	gsl_vector *y = gsl_vector_calloc(3); 
	gsl_vector *x = gsl_vector_calloc(3);
	gsl_matrix *A = gsl_matrix_calloc(3,3);

	gsl_vector_set(x,0,3.0);
	gsl_vector_set(x,1,4.0);
	gsl_matrix_set_identity(A);
	
	gsl_blas_dgemv(CblasNoTrans,1.0,A,x,0.0,y);
	printf("Hello World\n");
	
	//Write y to file to verify 
	FILE * f = fopen ("test.dat", "w"); 
	gsl_vector_fprintf (f, y, "%.5g"); 
	fclose (f);
	return 0;
}

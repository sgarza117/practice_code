#include <stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include "mygsl_vectops.h"
#include "mygsl_matops.h"
#include <math.h>
#include "perceptron.h"
gsl_rng *r; //global generator

int main (void)
{

	int n_hidden = 5;

	// Specify random num generator inputs
	const gsl_rng_type *T;
	double sigma = 0.1;
	
	T = gsl_rng_default;
	r = gsl_rng_alloc(T);	
 	
	// Define NN input data: x
	double  x_start = 0.0;
	double  x_end   = 10.0;
	double  x_step  = 0.01;
	double  x_nsteps = rint((x_end-x_start)/x_step + 1);
	gsl_vector * x = gsl_vector_calloc (x_nsteps);
	gsl_vector_set_all_range(x,x_start,x_end,x_step);

	// Define NN output data: y
	gsl_vector * y = gsl_vector_calloc (x_nsteps);
	sin_vect(y,x); //y = sin(x)

	//Initialize NN variables
	PERCEPTRON_GRAPH weights;
	PERCEPTRON_GRAPH gradients;

	// Allocate space for weight struct. Initialize to zero
	weights.layer1 = gsl_matrix_calloc(n_hidden,1);
	weights.layer2 = gsl_vector_calloc(n_hidden);
	weights.bias_layer1 = gsl_vector_calloc(n_hidden);
//	weights.bias_layer2; 

	// Set random weights
	gsl_matrix_set_all_gaus(weights.layer1,r,sigma);
	gsl_vector_set_all_gaus(weights.layer2,r,sigma);
	gsl_vector_set_all_gaus(weights.bias_layer1,r,sigma);
	weights.bias_layer2 = gsl_ran_gaussian(r,sigma); 
	
	// Initialize and define gradient struct. Set to zero
	gradients.layer1 = gsl_matrix_calloc(n_hidden,1);
	gradients.layer2 = gsl_vector_calloc(n_hidden);
	gradients.bias_layer1 = gsl_vector_calloc(n_hidden);
	gradients.bias_layer2 = 0.0; 

	// Initialize NN output struct
	PERCEPTRON_OUTPUT nn_output;

	int done = 0;	
	//Write y to file to verify 
	FILE * f = fopen ("test.dat", "w"); 
	gsl_vector_fprintf (f, y, "%.5g"); 
	fclose (f);
	return 0;
}

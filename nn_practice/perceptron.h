
typedef struct 
{
	gsl_matrix *layer1;
	gsl_vector *layer2;
	gsl_vector *bias_layer1;
	double      bias_layer2;
} PERCEPTRON_GRAPH;

typedef struct 
{
	double output_hat;
	gsl_vector *Z;
	gsl_vector *Q_prime;
} PERCEPTRON_OUTPUT;

void feedforward(const PERCEPTRON_GRAPH *weights, double input_k, PERCEPTRON_OUTPUT *output);

void back_prop(PERCEPTRON_GRAPH *gradient, const PERCEPTRON_GRAPH *weights,\
				double input_k, const PERCEPTRON_OUTPUT *output);

void update_gradient(PERCEPTRON_GRAPH *gradient);

void update_weights(PERCEPTRON_GRAPH *weights,const PERCEPTRON_GRAPH *gradient, double learning_rate);
